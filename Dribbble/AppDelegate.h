//
//  AppDelegate.h
//  Dribbble
//
//  Created by Vinicius Ricci on 24/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

