//
//  NSObject+APIDataManager.h
//  Dribbble
//
//  Created by Vinicius Ricci on 25/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import <Foundation/Foundation.h>


//@protocol APIDataManagerInput
//
//-(void) getShots:(NSString*) url eAccess_token:(NSString*) accessToken;
//
//@end

@interface APIDataManager : NSObject {
    
     //id<APIDataManagerInput>delegate;
    //APIDataManager *manager;
    
}
-(void) getShots;
//@property(nonatomic,assign)id<APIDataManagerInput>delegate;
//@property(nonatomic,assign) APIDataManager * manager;

@end




