//
//  ViewController.h
//  Dribbble
//
//  Created by Vinicius Ricci on 24/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIDataManager.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    APIDataManager *manager;
}


@property (nonatomic, assign) APIDataManager *manager;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *shotsArray;

@end


