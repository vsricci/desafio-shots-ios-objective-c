//
//  DetailsViewController.m
//  Dribbble
//
//  Created by Vinicius Ricci on 3/28/18.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import "DetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Shot.h"
@interface DetailsViewController ()

@end

@implementation DetailsViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.title = @"Details Shots";
        [self.imageShots sd_setImageWithURL:[NSURL URLWithString: self.shots.images.normal ]
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    [self.titleShots setText: self.shots.title];
    [self.descriptionShots setText: self.shots.desc];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
