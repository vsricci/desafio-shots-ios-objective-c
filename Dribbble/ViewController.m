//
//  ViewController.m
//  Dribbble
//
//  Created by Vinicius Ricci on 24/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "APIDataManager.h"
#import "Shot.h"
#import "Images.h"
#import "DetailsViewController.h"
@interface ViewController ()

@end



@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.shotsArray = [NSMutableArray new];

    [self shots];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_shotsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Shot *shotItem = [_shotsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = shotItem.title;
    
    return cell;
}


//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    if ([[segue identifier] isEqualToString:@"teste"]) {
//
//        DetailsViewController *detailsVC = [segue destinationViewController];
//        detailsVC.shotsArray = self.shotsArray;
//    }
//
//}


-(void) showDetailsShots:(Shot *) shots {
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Shot *itens = [_shotsArray objectAtIndex:indexPath.row];
    
    DetailsViewController *sec = [self.storyboard instantiateViewControllerWithIdentifier:@"teste"];
    sec.shots = itens;
    [self.navigationController pushViewController:sec animated:NO];

}


- (void) shots {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    
    
    NSURL *URL = [NSURL URLWithString:@"https://api.dribbble.com/v2/user/shots?access_token=debc2b9d118e2013de47901ba7df95cf8e658d42a640691803e45ed9bfcfc250"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
        
            NSLog(@"Error: %@", error);
        } else {
                NSData *data = [NSJSONSerialization dataWithJSONObject: responseObject options:NSJSONWritingPrettyPrinted error:nil];
            
                NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            for(id jsonShots in jsonObject) {
                
                Shot *shots = [[Shot alloc] init];
                
                [shots mts_setValuesForKeysWithDictionary:jsonShots];
                [self.shotsArray addObject:shots];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    [dataTask resume];
}
@end


