//
//  DetailsViewController.h
//  Dribbble
//
//  Created by Vinicius Ricci on 3/28/18.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shot.h"
@interface DetailsViewController : UIViewController {
    
}

@property (nonatomic, retain) IBOutlet UILabel *titleShots;
@property (nonatomic, retain) IBOutlet UILabel *descriptionShots;
@property (nonatomic, retain) IBOutlet UIImageView *imageShots;
@property (nonatomic, nonatomic) Shot *shots;


@end
