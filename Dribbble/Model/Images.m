//
//  NSObject+Images.m
//  Dribbble
//
//  Created by Vinicius Ricci on 26/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import "Images.h"

@implementation Images : MTSMotisObject

+ (NSDictionary*)mts_mapping
{
    return @{@"hidpi": mts_key(hidpi),
             @"normal": mts_key(normal),
             @"teaser": mts_key(teaser)
             };
}

@end
