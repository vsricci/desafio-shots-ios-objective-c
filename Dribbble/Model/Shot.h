//
//  NSObject+Shot.h
//  Dribbble
//
//  Created by Vinicius Ricci on 26/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//


#import <Motis/Motis.h>
#import <UIKit/UIKit.h>
#import "Images.h"

@interface Shot: MTSMotisObject {
    
}

    @property (nonatomic, strong) NSString *title;
    @property (nonatomic, strong) NSString *desc;
    @property (nonatomic, strong) Images *images;

@end
