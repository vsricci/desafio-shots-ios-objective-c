//
//  NSObject+Images.h
//  Dribbble
//
//  Created by Vinicius Ricci on 26/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import <Motis/Motis.h>
#import <UIKit/UIKit.h>
@interface Images: MTSMotisObject {
    
}

@property (nonatomic, strong) NSString *hidpi;
@property (nonatomic, strong) NSString *normal;
@property (nonatomic, strong) NSString *teaser;
    
@end
