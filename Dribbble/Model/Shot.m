//
//  NSObject+Shot.m
//  Dribbble
//
//  Created by Vinicius Ricci on 26/03/2018.
//  Copyright © 2018 Vinicius Ricci. All rights reserved.
//

#import "Shot.h"


@implementation Shot : MTSMotisObject


+ (NSDictionary*)mts_mapping
{
    return @{@"title": mts_key(title),
             @"description": mts_key(desc),
             @"images": mts_key(images)
            };
}

+ (NSDictionary*)mts_valueMappingForKey:(NSString*)key
{
    
        return @{@"title": mts_key(title),
                 @"description": mts_key(desc),
                 @"images": mts_key(images)
                 };
}

@end

